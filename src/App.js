import "./App.css";
import Navbar from "./component/Navbar/Navbar";
import Allproducts from "./component/Allproducts/Allproducts";
import React from "react";
import { Route, Routes, Link } from "react-router-dom";
import ProductInformation from "./component/ProductInformation/ProductInformation";
import Cart from "./component/Cart/Cart";

function App() {




  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Allproducts />} />
        <Route exact path="/:idName" element={<ProductInformation />} />
        <Route exact path="/cart" element={<Cart />} />
      </Routes>
    </div>
  );
}

export default App;
