import React from "react";
import "./Productcard.css";
import { useNavigate } from "react-router-dom";

function Productcard(props) {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`${id}`);
  };

  const { id, category, description, image, price, title, rating } = props.data;
  return (
    <div className="productcard" onClick={handleClick}>
      {/* <Link to={}  style={{ textDecoration: "none" }}>  */}
      <img className="productImage" src={image} alt={title}></img>
      <span className="title">{title}</span>
      <div className="productDescription">{description.slice(0, 100)}</div>
      <h3 className="category">
        {" "}
        <span>{category}</span>
      </h3>
      <div className="customerRatings">
        <h4 className="rating">
          <i className="fa-solid fa-star"></i>
          {rating.rate}
        </h4>
        <h4 style={{ color: "blue" }}>({rating.count})</h4>
      </div>
      <span className="price">${price}</span>
      {/* </Link> */}
    </div>
  );
}

export default Productcard;
