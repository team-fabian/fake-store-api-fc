import React from "react";
import "./CartProductTab.css";

export default function CartProductTab(props) {
  return (
    <div className="cartProduct">
      <img className="productImage" src={props.data.image}></img>
      <div>
        <h3>{props.data.title}</h3>
        <h3> $ {props.data.price}</h3>
        <h3>Qty:{props.data.quantity}</h3>
      </div>

      <div className="total">
        <h3>Total:{props.data.quantity * props.data.price}</h3>
      </div>
    </div>
  );
}
