import Productcard from "../Productcard/Productcard";
import "./Allproducts.css";
import React, { useState, useEffect } from "react";

function Allproducts() {
  const API_STATES = {
    LOADING: "loading",

    LOADED: "loaded",

    ERROR: "error",
  };

  const [ProductData, setProductData] = useState([]);

  const [Status, setStatus] = useState(API_STATES.LOADING);

  const [ErrorMessage, setErrorMessage] = useState("");

  const url = "https://fakestoreapi.com/products";

  const FetchData = (url) => {
    setStatus(API_STATES.LOADING);

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setProductData(data);
        setStatus(API_STATES.LOADED);
      })
      .catch((error) => {
        setErrorMessage(
          "An API error occurred. Please try again in a few minutes."
        );
        setStatus(API_STATES.ERROR);
      });
  };

  useEffect(() => {
    FetchData(url);
  }, []);

  let AllData = ProductData;

  const allProducts = AllData.map((product) => {
    return <Productcard key={product.id} data={product} />;
  });

  return (
    <div>
      <>
        {Status === API_STATES.LOADING && <div className="loader"></div>}

        {Status === API_STATES.ERROR && (
          <div className="error">
            <h1>{ErrorMessage}</h1>
          </div>
        )}

        {Status === API_STATES.LOADED && AllData.length === 0 && (
          <div className="error">
            <h1>
              No products available at the moment. Please try again later.
            </h1>
          </div>
        )}
        {Status === API_STATES.LOADED && (
          <div className="allProducts">{allProducts}</div>
        )}
      </>
    </div>
  );
}

export default Allproducts;
