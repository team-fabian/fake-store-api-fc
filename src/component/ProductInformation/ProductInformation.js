import React, { useState, useEffect } from "react";
import "./ProductInformation.css";
import { useParams } from "react-router-dom";
import Link from "react-router-dom";

function ProductInformation() {
  const { idName } = useParams();

  const API_STATES = {
    LOADING: "loading",

    LOADED: "loaded",

    ERROR: "error",
  };

  const [CurrentProduct, setCurrentProduct] = useState([]);

  const [ProductStatus, setProductStatus] = useState(API_STATES.LOADING);

  const [ProductErrorMessage, setProductErrorMessage] = useState("");

  const url = `https://fakestoreapi.com/products/ ${idName}`;

  const fetchProductDetails = (url) => {
    setProductStatus(API_STATES.LOADING);

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setCurrentProduct(data);
        setProductStatus(API_STATES.LOADED);
      })
      .catch((error) => {
        setProductErrorMessage(
          "An API error occurred. Please try again in a few minutes."
        );
        setProductStatus(API_STATES.ERROR);
      });
  };
  useEffect(() => {
    fetchProductDetails(url);
  }, []);

  return (
    <>
      {ProductStatus === API_STATES.LOADING && <h1>Loading Product Details</h1>}

      {ProductStatus === API_STATES.ERROR && (
        <div className="error">
          <h1>{ProductErrorMessage}</h1>
        </div>
      )}

      {ProductStatus === API_STATES.LOADED && CurrentProduct.length !== 0 && (
        <div className="productInformation">
          <div className="productImg">
            <img className="img" src={CurrentProduct.image}></img>
          </div>

          <div className="information">
            <h1>{CurrentProduct.title}</h1>

            <h4 className="category"> Category:{CurrentProduct.category}</h4>
            <hr></hr>
            <div className="info">
              <h4 className="stars">
                Customer Rating: {CurrentProduct.rating.rate}{" "}
                <i className="fa-solid fa-star"></i>{" "}
              </h4>
              <h4 className="reviewCount">
                <i class="fa-sharp fa-solid fa-users"></i>{" "}
                {CurrentProduct.rating.count} reviews
              </h4>
            </div>
            <div className="buyingInfo">
              <h4 className="Productprice">${CurrentProduct.price}</h4>
              <h4 className="cart">
                <i class="fa-sharp fa-solid fa-cart-shopping"></i>Add to cart
              </h4>
            </div>

            <hr></hr>
            <h4 className="desc">{CurrentProduct.description}</h4>
          </div>
        </div>
      )}
    </>
  );
}
export default ProductInformation;
