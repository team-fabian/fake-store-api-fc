import "./Navbar.css";
import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <>
      <div className="navbar">
        <div className="brand">
          <i className="fa-solid fa-shirt"></i>
          Myntra
        </div>
        <div className="allButtons">
          <Link to={"/cart"}>
            <button className="cartButton">
              <i class="fa-solid fa-cart-shopping"></i>Cart
            </button>
          </Link>
        </div>
      </div>
    </>
  );
}

export default Navbar;
